# Automatic_emergency_light
![image](https://gitlab.com/sherindavid/automatic_emergency_light/-/raw/main/ael.png?ref_type=heads)


This project is aimed to design an Automatic emergency light which is operated by the switching action of relay and it illuminate when there is a power outage and charge the battery when the power supply is on. A protection module is integrated to safeguard the battery from overcharging or discharging.

# Prerequisites
- Hi-link 12V 5W
- Relay 12V
- LM2596S DC-DC Buck Converter
- 18650 Lithium Battery Charger Board Protection Module

# Getting Started 

- Setup the circuit as per shown in the [wiring diagram](https://gitlab.com/sherindavid/automatic_emergency_light/-/blob/main/Hardware/wiring_diagram.png?ref_type=heads)

- Check that the charging current and voltage of the system are sufficient as per our needs.

- Test the circuit and verify that its output satisfies our requirements and that all connections are correct.

- Assemble the components into a suitable enclosure to protect them from environmental factors and ensure user safety

- Install the emergency light system in the desired location, ensuring that it is easily accessible during emergencies.

# Contributing

Instructions coming up soon.



